# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-02-06 15:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('worksheet', '0204_deduccionesunicasarchivo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deduccionesunicasarchivo',
            old_name='planila',
            new_name='planilla',
        ),
    ]
