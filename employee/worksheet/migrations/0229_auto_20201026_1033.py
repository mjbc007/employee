# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-10-26 16:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('worksheet', '0228_auto_20201023_1003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='planilla',
            name='descripcion',
            field=models.CharField(max_length=500),
        ),
    ]
