# -*- coding: utf-8 -*-
# Generated by Django 1.11.26 on 2020-01-06 19:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('worksheet', '0193_auto_20200106_1212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deduccionindividualsubdetalle',
            name='deduccionplanilladetalle',
        ),
    ]
