# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-08-28 16:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('worksheet', '0212_auto_20200828_0811'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingresoempleado',
            name='empresa_reg',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='worksheet.Empresa'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ingresotipo',
            name='empresa_reg',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='worksheet.Empresa'),
            preserve_default=False,
        ),
    ]
